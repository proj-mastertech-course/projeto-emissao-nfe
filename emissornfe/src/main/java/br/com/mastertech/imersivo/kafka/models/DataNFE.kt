package br.com.mastertech.imersivo.kafka.models
// solicitacao: 1 emissao, 2: consulta
data class DataNFE(val identidade: String, val valor: Double, val solicitacao: Int)