package br.com.mastertech.imersivo.emissornfe.controller

import br.com.mastertech.imersivo.emissornfe.service.PublishDataNFE
import br.com.mastertech.imersivo.kafka.models.MapperDataNFE
import br.com.mastertech.imersivo.kafka.models.dto.RqDataNFE
import br.com.mastertech.imersivo.kafka.models.dto.RsConsultaNFE
import br.com.mastertech.imersivo.kafka.models.dto.RsEmissaoNFE
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
class EmissorNFEController {

    @Autowired
    lateinit var publish: PublishDataNFE

    @Autowired
    lateinit var controllerConsultarNFE: ConsultarNFE

    @PostMapping("/nfe/emitir")
    @ResponseStatus(HttpStatus.CREATED)
    fun emitirNFE(@RequestBody rqDataNFE: RqDataNFE) : RsEmissaoNFE {
        println(rqDataNFE)
        publish.publish(MapperDataNFE.fromRqDataNFEToModel(rqDataNFE, 1))
        return RsEmissaoNFE()
    }


    @GetMapping("/nfe/consultar/{cpf_cnpj}")
    @ResponseStatus(HttpStatus.OK)
    fun consultarNFE(@PathVariable(name = "cpf_cnpj") doc: String): List<RsConsultaNFE>? {
        val nfes = controllerConsultarNFE.consulta(doc)
        return nfes
    }

}