package br.com.mastertech.imersivo.emissornfe.service

import br.com.mastertech.imersivo.kafka.models.DataNFE
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Service

@Service
class PublishDataNFE {
    companion object {
        private const val TOPIC = "chrislucas-biro-1"
    }

    @Autowired
    private lateinit var publisher: KafkaTemplate<String, DataNFE>

    fun publish(data: DataNFE) = publisher.send(TOPIC, data)
}