package br.com.mastertech.imersivo.kafka.models.dto

import com.fasterxml.jackson.annotation.JsonProperty

class RsConsultaNFE(@JsonProperty("identidade") val identidade: String
                    , @JsonProperty("valor") val valor: Double
                    , @JsonProperty("status") val status: String
                    , @JsonProperty("nfe") val nfe: RsValuesNFE)