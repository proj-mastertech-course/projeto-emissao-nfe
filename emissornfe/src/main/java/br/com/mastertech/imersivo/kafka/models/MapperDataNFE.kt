package br.com.mastertech.imersivo.kafka.models

import br.com.mastertech.imersivo.kafka.models.dto.RqDataNFE

object MapperDataNFE {

    @JvmStatic
    fun fromRqDataNFEToModel(rq: RqDataNFE, solicitacao: Int) : DataNFE =
            DataNFE(rq.identidade, rq.valor, solicitacao)

}