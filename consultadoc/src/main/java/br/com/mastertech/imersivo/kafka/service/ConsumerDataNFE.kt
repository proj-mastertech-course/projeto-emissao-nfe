package br.com.mastertech.imersivo.kafka.service


import br.com.mastertech.imersivo.kafka.BrazilDocHelper
import br.com.mastertech.imersivo.kafka.controller.ClientApiReceitaWs
import br.com.mastertech.imersivo.kafka.models.DataNFE
import br.com.mastertech.imersivo.kafka.models.MessageLogNFE
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.messaging.handler.annotation.Payload
import org.springframework.stereotype.Component

@Component
class ConsumerDataNFE {
    @Autowired
    lateinit var publisher: PublisherDataNFE

    @Autowired
    lateinit var service: ServiceNFE

    @Autowired
    lateinit var clientApiReceitaWs: ClientApiReceitaWs

    @KafkaListener(topics = ["chrislucas-biro-1"], groupId = "chrisluccas1")
    fun listener(@Payload dataNFE: DataNFE) {
        println("Recebido os dados de: $dataNFE")
        val tipo = BrazilDocHelper.isCPFOrCNPJ(dataNFE.identidade)
        if (tipo > -1) {
            if (tipo == 0) {
                val nfeModel = service.emitirNFEPessoaFisica(dataNFE)
                println("Salvando o NFE PF: $nfeModel")
            } else {
                val cnpj = dataNFE.identidade.replace(Regex("\\D+"), "")
                val rsCompany = clientApiReceitaWs.get(cnpj)
                val aplicarSimplesNacional = rsCompany.capitalSocial.toDouble() >= 10000000000.0
                val nfeModel = service.emitirNFEPessoaJuridica(dataNFE, aplicarSimplesNacional)
                println("Salvando o NFE PJ: $nfeModel")
            }
            publisher.publish(fromConsumerMessageToProducer(dataNFE))
        } else {
            println("Documento informado não é um CNPJ nem CPF")
        }
    }

    private fun fromConsumerMessageToProducer(dataNFE: DataNFE) : MessageLogNFE =
            dataNFE.run {
                MessageLogNFE(identidade = this.identidade, valor = this.valor
                        , solicitacao = this.solicitacao)
            }
}