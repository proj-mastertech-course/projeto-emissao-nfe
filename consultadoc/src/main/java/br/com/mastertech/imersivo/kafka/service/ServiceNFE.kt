package br.com.mastertech.imersivo.kafka.service

import br.com.mastertech.imersivo.kafka.models.DataNFE
import br.com.mastertech.imersivo.kafka.models.MapperNfe
import br.com.mastertech.imersivo.kafka.models.NfeBO
import br.com.mastertech.imersivo.kafka.models.NfeModel
import br.com.mastertech.imersivo.kafka.repository.NfeRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ServiceNFE {

    @Autowired
    lateinit var repository: NfeRepository

    fun emitirNFEPessoaFisica(dataNFE: DataNFE) : NfeModel {
        val nfeBO = NfeBO(valorInicial = dataNFE.valor, documento = dataNFE.identidade)
        return salvar(nfeBO)
    }

    fun emitirNFEPessoaJuridica(dataNFE: DataNFE, aplicarSimplesNacional: Boolean): NfeModel {
        val nfe = NfeBO(valorInicial = dataNFE.valor
                , tipoPessoa = NfeBO.TIPO_PESSOA_JURIDICA
                , aplicarSimplesNacional = aplicarSimplesNacional
                , status = "complete"
                , documento = dataNFE.identidade)
        return salvar(nfe)
    }

    private fun salvar(nfeBO: NfeBO) : NfeModel {
        val nfeModel = MapperNfe.fromBOToModel(nfeBO)
        return repository.save(nfeModel)
    }

    fun consultarNfes(doc: String): List<NfeModel> =
            repository.getNefModelsByDocumento(doc)
}