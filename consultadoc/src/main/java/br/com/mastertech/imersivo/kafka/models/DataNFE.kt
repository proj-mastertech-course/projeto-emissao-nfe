package br.com.mastertech.imersivo.kafka.models

// solicitacao: 1 emissao, 2: consulta
// Modelo enviado pelo Kafka
data class DataNFE(val identidade: String = "", val valor: Double = 0.0, val solicitacao: Int = 0)