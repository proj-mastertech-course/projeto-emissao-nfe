package br.com.mastertech.imersivo.kafka.service

import br.com.mastertech.imersivo.kafka.models.MessageLogNFE
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.kafka.support.SendResult
import org.springframework.stereotype.Service
import org.springframework.util.concurrent.ListenableFuture

@Service
class PublisherDataNFE {

    companion object {
        private const val TOPIC = "chrislucas-biro-2"
    }

    @Autowired
    private lateinit var publisher: KafkaTemplate<String, MessageLogNFE>

    fun publish(message: MessageLogNFE): ListenableFuture<SendResult<String, MessageLogNFE>>? = publisher.send(TOPIC, message)

}