package br.com.mastertech.imersivo.kafka.models

import javax.persistence.*

@Entity
@Table(name = "nfe")
data class NfeModel(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long = 0,
        @Column(name = "valor_inicial")
        val valorInicial: Double = 0.0,
        @Column(name = "valor_irff")
        var valorIRRF: Double = 0.0,
        @Column(name = "valor_crll")
        var valorCSLL: Double = 0.0,
        @Column(name = "valor_pis_cofins")
        var valorCofins: Double = 0.0,
        @Column(name = "valor_final")
        var valorFinal: Double = 0.0,
        @Column(name = "status")
        var status: String = "",
        @Column(name = "documento")
        var documento: String = ""
)