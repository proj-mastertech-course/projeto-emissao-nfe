package br.com.mastertech.imersivo.kafka.models.dto

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*


data class RsCompany(
        @JsonProperty("atividade_principal") var atividadePrincipal: ArrayList<RqAtividadePrincipal>
        , @JsonProperty("data_situacao") var dataSituacao: String? = null
        , var complemento: String? = null
        , var nome: String? = null
        , var uf: String? = null
        , var telefone: String? = null
        , var situacao: String? = null
        , var bairro: String? = null
        , var logradouro: String? = null
        , var numero: String? = null
        , var cep: String? = null
        , var municipio: String? = null
        , var porte: String? = null
        , var abertura: String? = null
        , @JsonProperty("natureza_juridica") var naturezaJuridica: String? = null
        , var cnpj: String? = null
        , @JsonProperty("ultima_atualizacao") var ultimaAtualizacao: String? = null
        , var status: String? = null
        , var tipo: String? = null
        , var fantasia: String? = null
        , var email: String? = null
        , var efr: String? = null
        , @JsonProperty("motivo_situacao") var motivoSituacao: String? = null
        , @JsonProperty("situacao_especial") var situacaoEspecial: String? = null
        , @JsonProperty("data_situacao_especial") var dataSituacaoEspecial: String? = null
        , @JsonProperty("capital_social") var capitalSocial: String)
