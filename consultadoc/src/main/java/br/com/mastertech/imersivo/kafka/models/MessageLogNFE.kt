package br.com.mastertech.imersivo.kafka.models

import java.text.SimpleDateFormat
import java.util.*

data class MessageLogNFE(val id: Long = 0, val identidade: String, val valor: Double,
                         val solicitacao: Int, val data: Long = System.currentTimeMillis()) {

    companion object {
        private const val PATTERN_LOG = "yyyy-MM-dd HH:mm:ss"
        private val simpleDateFormat = SimpleDateFormat(PATTERN_LOG)
    }

    override fun toString(): String {
        val (tipoSolicitacao: String, mensagem: String) = if (solicitacao == 1)  {
            Pair("Emissão"
                    , "$identidade acaba de pedir a emissão de uma NF no valor de R\$ $valor!")
        } else  {
            Pair("Consulta"
                    , "$identidade acaba de pedir os dados das suas notas fiscais.!")
        }

        return String.format("[%s][%s]:%s"
            , simpleDateFormat.format(Date(data))
            , tipoSolicitacao
            , mensagem
        )
    }
}