package br.com.mastertech.imersivo.kafka.models

import br.com.mastertech.imersivo.kafka.models.dto.RsNFE
import br.com.mastertech.imersivo.kafka.models.dto.RsValueNFE

object MapperNfe {

    @JvmStatic
    fun fromBOToModel(nfeBO: NfeBO) : NfeModel =
            nfeBO.run {
                NfeModel(
                        valorInicial = this.valorInicial
                    , valorIRRF = this.valorIRRF
                    , valorCSLL = this.valorCSLL
                    , valorCofins = this.valorCofins
                    , valorFinal = this.valorFinal
                    , status = this.status
                    , documento = this.documento
                )
            }

    private fun fromModelToResponseModel(nfeModel: NfeModel) : RsNFE {
        return nfeModel.run {
            val valuesNfe = RsValueNFE(
                    nfeModel.id
                ,  nfeModel.valorInicial
                ,  nfeModel.valorIRRF
                ,  nfeModel.valorCSLL
                ,  nfeModel.valorCofins
                ,  nfeModel.valorFinal
            )
            val rs = RsNFE(
                    nfeModel.documento
                , nfeModel.valorInicial
                , nfeModel.status
                , valuesNfe
            )
            rs
        }
    }

    fun fromModelsToResponseModels(list: List<NfeModel>) : MutableList<RsNFE> {
        val newList = arrayListOf<RsNFE>()
        list.forEach {
            newList.add(fromModelToResponseModel(it))
        }
        return newList
    }
}