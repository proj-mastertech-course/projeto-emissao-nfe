package br.com.mastertech.imersivo.kafka.models.dto

data class RqAtividadePrincipal(val text: String, val code: String)
