package br.com.mastertech.imersivo.kafka.models.dto

import com.fasterxml.jackson.annotation.JsonProperty

data class RsValueNFE(@JsonProperty("id") val id: Long
                      ,@JsonProperty("valorInicial") val valorInicial: Double
                      ,@JsonProperty("valorIRRF") val valorIRRF: Double
                      ,@JsonProperty("valorCSLL") val valorCSLL: Double
                      ,@JsonProperty("valorCofins") val valorCofins: Double
                      ,@JsonProperty("valorFinal") val valorFinal: Double)