package br.com.mastertech.imersivo.kafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class ConsultadocApplication {
	public static void main(String[] args) {
		SpringApplication.run(ConsultadocApplication.class, args);
	}
}
