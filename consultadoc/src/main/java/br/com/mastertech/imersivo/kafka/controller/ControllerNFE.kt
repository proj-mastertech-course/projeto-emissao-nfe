package br.com.mastertech.imersivo.kafka.controller

import br.com.mastertech.imersivo.kafka.BrazilDocHelper
import br.com.mastertech.imersivo.kafka.models.MapperNfe
import br.com.mastertech.imersivo.kafka.models.MessageLogNFE
import br.com.mastertech.imersivo.kafka.models.dto.RsNFE
import br.com.mastertech.imersivo.kafka.service.PublisherDataNFE
import br.com.mastertech.imersivo.kafka.service.ServiceNFE
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
class ControllerNFE {

    @Autowired
    private lateinit var service: ServiceNFE

    @Autowired
    lateinit var publisher: PublisherDataNFE

    @GetMapping("/nfe/consultar/{cpf_cnpj}")
    @ResponseStatus(HttpStatus.OK)
    fun get(@PathVariable(name = "cpf_cnpj") doc: String) : List<RsNFE> {
        println("consulta $doc, ${BrazilDocHelper.aplicarFormatacao(doc)}")
        val nfes = MapperNfe.fromModelsToResponseModels(service.consultarNfes(BrazilDocHelper.aplicarFormatacao(doc)))
        val acc = nfes.fold(0.0) {
            acc, rsNFE ->
            val p: Double = rsNFE.nfe?.let { it.valorFinal } ?: 0.0
            acc + p
            p
        }
        publisher.publish(MessageLogNFE(identidade = doc, valor = acc, solicitacao = 2))
        return nfes
    }
}