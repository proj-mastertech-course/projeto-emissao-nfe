package br.com.mastertech.imersivo.kafka.models

data class NfeBO(val id: Long = 0
                 , val valorInicial: Double
                 , val tipoPessoa: String = TIPO_PESSOA_FISICA
                 , private val aplicarSimplesNacional: Boolean = false
                 , val status: String = "pending"
                 , val documento: String
) {
    companion object {
        const val TIPO_PESSOA_FISICA = "PF"
        const val TIPO_PESSOA_JURIDICA = "PJ"
    }

    var valorIRRF: Double = 0.0
    var valorCSLL: Double = 0.0
    var valorCofins: Double = 0.0
    var valorFinal: Double = 0.0

    init {
        if (tipoPessoa == TIPO_PESSOA_JURIDICA) {
            if(aplicarSimplesNacional) {
                aplicaRegraSimplesNacional()
            }
            else {
                aplicaRegrasNaoOptanteSimplesNacional()
            }
        }
    }

    private fun aplicaRegraSimplesNacional() {
        this.valorIRRF = valorInicial * 0.015
        this.valorFinal = valorInicial - this.valorIRRF
    }

    private fun aplicaRegrasNaoOptanteSimplesNacional() {
        this.valorIRRF = valorInicial * 0.015
        this.valorCSLL = valorInicial * 0.030
        this.valorCofins = valorInicial * 0.0065
        this.valorFinal = valorInicial - (valorCSLL + valorIRRF + valorCofins)
    }

}