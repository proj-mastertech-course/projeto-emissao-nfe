package br.com.mastertech.imersivo.kafka

object BrazilDocHelper {
    private val patternCPF = Regex("(\\d{3}\\.){2}\\d{3}-\\d{2}")
    private val patternCNPJ = Regex("(\\d{2,3}\\.){2}\\d{3}/\\d{4}-\\d{2}")

    @JvmStatic
    private fun isCPFOrCNPJFormat(doc: String) : Boolean = doc.matches(patternCNPJ) or doc.matches(patternCPF)

    @JvmStatic
    fun isCPFOrCNPJ(doc: String) : Int {
        return if (!isCPFOrCNPJFormat(doc)) {
            -1
        } else if(doc.matches(patternCPF)) {
            0
        } else{
            1
        }
    }

    @JvmStatic
    fun aplicarFormatacao(doc: String): String {
        return if (doc.length == 11) {
            doc.replace("(\\d{3})(\\d{3})(\\d{3})(\\d{2})".toRegex(), "$1.$2.$3-$4")
        } else {
            doc.replace("(\\d{2,3})(\\d{3})(\\d{3})(\\d{4})(\\d{2})".toRegex(), "$1.$2.$3/$4-$5")
        }
    }
}