package br.com.mastertech.imersivo.kafka.repository

import br.com.mastertech.imersivo.kafka.models.NfeModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface NfeRepository : JpaRepository<NfeModel, Long> {
    fun getNefModelsByDocumento(doc: String): List<NfeModel>
}