package br.com.mastertech.imersivo.kafka.models

import javax.persistence.*

@Entity
@Table(name = "log_nfe")
data class LogNFE(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long = 0,
        @Column(name = "identidade")
        var identidade: String,
        @Column(name = "valor")
        var valor: Double,
        @Column(name = "solicitacao")
        var solicitacao: Int,
        @Column(name = "data")
        var data: Long)