package br.com.mastertech.imersivo.kafka.controller

import br.com.mastertech.imersivo.kafka.service.LogNFEService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController


@RestController
class LogNFEController {

    @Autowired
    private lateinit var service: LogNFEService

    @GetMapping("get/{docUser}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    fun getLog(@PathVariable("docUser") identity: String) {
        val logs = service.getLogs(identity)
        println("Logs: $logs")
    }

}