package br.com.mastertech.imersivo.kafka.repository

import br.com.mastertech.imersivo.kafka.models.LogNFE
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface LogNFERepository : JpaRepository<LogNFE, Long> {
    fun getLogNFEByIdentidade(identidade: String) : List<LogNFE>
}