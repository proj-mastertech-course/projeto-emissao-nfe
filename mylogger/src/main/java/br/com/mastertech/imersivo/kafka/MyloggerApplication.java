package br.com.mastertech.imersivo.kafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class MyloggerApplication {
	public static void main(String[] args) {
		SpringApplication.run(MyloggerApplication.class, args);
	}
}
