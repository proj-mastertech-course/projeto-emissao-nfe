package br.com.mastertech.imersivo.kafka.models

object MapperNFE {

    @JvmStatic
    fun fromKafkaMessageToEntity(messageLog: MessageLogNFE) : LogNFE =
            messageLog.run {
                LogNFE(identidade = this.identidade, valor = this.valor, solicitacao = this.solicitacao
                        , data = this.data)
            }


    @JvmStatic
    fun fromEntityToKafkaMessage(logNFE: LogNFE) : MessageLogNFE =
            logNFE.run {
                MessageLogNFE(this.id, this.identidade, this.valor, this.solicitacao, this.data)
            }
}