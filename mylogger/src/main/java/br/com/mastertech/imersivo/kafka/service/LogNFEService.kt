package br.com.mastertech.imersivo.kafka.service

import br.com.mastertech.imersivo.kafka.models.LogNFE
import br.com.mastertech.imersivo.kafka.repository.LogNFERepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class LogNFEService {

    @Autowired
    private lateinit var repository: LogNFERepository


    fun getLogs(identity: String) : List<LogNFE> =
            repository.getLogNFEByIdentidade(identity)

}