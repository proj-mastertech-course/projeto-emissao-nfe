package br.com.mastertech.imersivo.kafka.service


import br.com.mastertech.imersivo.kafka.models.MapperNFE
import br.com.mastertech.imersivo.kafka.models.MessageLogNFE
import br.com.mastertech.imersivo.kafka.repository.LogNFERepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.messaging.handler.annotation.Payload
import org.springframework.stereotype.Component

@Component
class ConsumerDataNFE {

    @Autowired
    lateinit var repository: LogNFERepository

    @KafkaListener(topics = ["chrislucas-biro-2"], groupId = "chrisluccas")
    fun listener(@Payload messageLogNFE: MessageLogNFE) {
        println("Recebido os dados de: $messageLogNFE")
        var log = MapperNFE.fromKafkaMessageToEntity(messageLogNFE)
        log = repository.save(log)
        println("Log que sera salvo: $log")
        val newMessageNFE = MapperNFE.fromEntityToKafkaMessage(log)
        println("Novo Log registrado: $newMessageNFE")
    }
}