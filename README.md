# projeto-emissao-nfe

# Emissão de NFE.

## Descrição
A Receita Federal quer disponibilizar um sistema público para emissão de notas fiscais, ela quer um endpoint que uma entidade passe seu CPF/CNPJ e o valor total, e o sistema automáticamente realize a emissão da nota fiscal.

Mas lógico, a emissão é algo que pode levar um tempo já que precisa ser validada com os orgãos competentes. Então ela pediu que a emissão fosse realizada em um microsserviço reativo, o processo vai acontecer em 2 etapas sendo elas:
- A entidade passa o CPF/CNPJ dela e o valor da nota a um microsserviço e o microsserviço envia esses dados ao Kafka
- Um segundo microsserviço deve consumir do kafka e realizar a operação de emissão de nota fiscal e depois atualizar no banco de dados o status da nota fiscal para emitida.

Importante!
Por ser um serviço do governo, é muito importante que tenha log de acessos, logs esses que devem ser enviados via Kafka a **um microsserviço dedicado a gravar os logs** em disco.

Obs: Como é um serviço muito usado, é importante que esteja integrado com o ZipKin

## Calculo de impostos
### Considerando uma prestação de serviço no valor de 1000 reais
 - Para emitir NF Pessoa Física, se emite valor cheio sem desconto de impostos. Pois será pago o ISS que será recolhido. R$ 1.000,00.
 - Para emitir NF Pessoa Jurídica **optante pelo simples nacional**. Será descontado somente o IRRF (1,5%) R$ 15,00, valor da NF líquido será R$ 985,00.
 - Para Emitir NF Pessoa Jurídica **não optante pelo simples nacional**. Será  descontado o IRRF (1,5%) , CSLL (3%) , PIS /Cofins (0,65%). Seria R$ 948,50 valor líquido.

Obs: Como **a api que você tera acesso não tem a informação se a empresa é optante pelo simples nacional ou não**, adote como optante todas aquelas que tiverem o captal social abaixo de **1Mi (1 Milhão de Reais)**.

## API de consulta dos dados da empresa:
http://www.receitaws.com.br/v1/cnpj/45997418000153

## Endpoints:

 ### POST /nfe/emitir
Solicita a emissão de uma nova nota fiscal

**Request Body**
```json
{
    "identidade": "<CPF ou CNPJ>",
    "valor": 1000.00
}
```

**Response 201**
```json
{
    "status": "pending"
}
```

 ### GET /nfe/consultar/{cpf/cnpj}
Consulta o status das notas fiscais.

**Response 200**
```json
[
    {
        "identidade": "<CPF ou CNPJ>",
        "valor": 1000.00,
        "status": "pending",
        "nfe": null
    },
    {
        "identidade": "<CPF ou CNPJ>",
        "valor": 1000.00,
        "status": "complete",
        "nfe": {
            "id": 1,
            "valorInicial": 1000.00,
            "valorIRRF": 15.00,
            "valorCSLL": 30.00,
            "valorCofins": 6.50,
            "valorFinal": 948.50,
        }
    },
]
```

## Exemplo de log

[2020-05-14T12:10:00Z] [Emissão]: 45997418000153 acaba de pedir a emissão de uma NF no valor de R$ 1.000,00!

[2020-05-14T12:10:00Z] [Consulta]: 45997418000153 acaba de pedir os dados das suas notas fiscais.